from django.contrib import admin
from .models import Chest, Item
# Register your models here.

admin.site.register(Chest)
admin.site.register(Item)
