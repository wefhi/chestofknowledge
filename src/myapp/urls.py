from django.conf.urls import url, include
from . import views
from . import serializers
from . import routers

app_name = 'myapp'

urlpatterns = [
    url(r'^api/', include(routers.router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^about-us/', views.aboutus, name="about-us"),
    url(r'^contact/', views.contact, name="contact"),
    url(r'^$', views.ChestList.as_view(), name='chest-list'),
    url(r'^chest/(?P<pk>[0-9]+)/$', views.ChestDetailView.as_view(), name="chest-detail"),
    url(r'^create_chest/$', views.ItemCreateView.as_view(template_name="item_form.html"), name="chest-create"),

]
