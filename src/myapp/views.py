from django.views import generic
from django.views.generic.edit import CreateView, UpdateView
from myapp.models import Chest, Item
from django.views.generic.detail import DetailView
from django.shortcuts import render


class ChestList(generic.ListView):
    template_name = "website/index.html"
    context_object_name = "chest_list"

    def get_queryset(self):
        return Chest.objects.all()

class ChestDetailView(DetailView):
    model = Chest
    template_name = 'website/chestDetail.html'

class ItemCreateView(CreateView):
    model = Item
    fields = ['name', 'language', 'chest', 'icon', 'obj']

def aboutus(request):
    return render(request, 'website/o_nas.html')

def contact(request):
    return render(request, 'website/kontakt.html')
