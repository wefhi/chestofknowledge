from django.db import models

# Create your models here.

class Photograph(models.Model):
    photo = models.FileField()


class Chest(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=30)
    language = models.CharField(max_length=30)
    icon = models.FileField()
    chest = models.ForeignKey(Chest, related_name='items', on_delete=models.CASCADE)
    obj = models.FileField()
    def __str__(self):
        return self.name
